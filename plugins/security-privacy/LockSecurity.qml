/*
 * This file is part of system-settings
 *
 * Copyright (C) 2013-2016 Canonical Ltd.
 *
 * Contact: Iain Lane <iain.lane@canonical.com>
 *          Jonas G. Drange <jonas.drange@canonical.com>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as published
 * by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranties of
 * MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import Biometryd 0.0
import GSettings 1.0
import QtQuick 2.4
import QtQuick.Layouts 1.1
import SystemSettings 1.0
import SystemSettings.ListItems 1.0 as SettingsListItems
import Lomiri.Components 1.3
import Lomiri.Components.ListItems 1.3 as ListItems
import Lomiri.Components.Popups 1.3
import Lomiri.Settings.Fingerprint 0.1
import Lomiri.SystemSettings.SecurityPrivacy 1.0

ItemPage {
    id: page
    property LomiriSecurityPrivacyPanel securityPrivacy
    objectName: "lockSecurityPage"
    title: i18n.tr("Lock security")

    // The user can still press the main "back" button or other buttons on the
    // page while the "change password" dialog is up.  This is because the
    // dialog is not guaranteed to cover the whole screen; consider the case of
    // turning a device to landscape mode.  We'd rather not have the password
    // changing operation interrupted by destroying the dialog out from under
    // it.  So we make sure the whole page and header back button are disabled
    // while the dialog is working.
    enabled: dialog === null
    flickable: scrollWidget
    head.backAction: Action {
        iconName: "back"
        enabled: page.enabled
        onTriggered: {
            pageStack.pop();
        }
    }

    property var dialog: null
    property int enrolledFingerprints: 0

    function openDialog(newMethod) {
        dialog = PopupUtils.open(dialogComponent, page)
        // Set manually rather than have these be dynamically bound, since
        // the security type can change out from under us, but we don't
        // want dialog to change in that case.
        dialog.oldMethod = securityPrivacy.securityType
        dialog.newMethod = newMethod

        dialog.accepted.connect(function() {
            unlockMethod.setMethod(securityPrivacy.securityType)
            PopupUtils.close(dialog)
            if (dialog.newMethod === LomiriSecurityPrivacyPanel.Passcode) {
                openPinPromptsPage()
            }
        });
    }

    function openPinPromptsPage() {
        var incubator = pageStack.addPageToNextColumn(page, Qt.resolvedUrl("PincodePrompts.qml"))
        if (incubator.status === Component.Ready) {
            incubator.object.passCodeChangeRequested.connect(function() {
                openDialog(LomiriSecurityPrivacyPanel.Passcode)
            });
        } else {
            incubator.onStatusChanged = function(status) {
                if (status === Component.Ready) {
                    incubator.object.passCodeChangeRequested.connect(function() {
                        openDialog(LomiriSecurityPrivacyPanel.Passcode)
                    });
                }
            }
        }

    }

    RegExpValidator {
        id: passcodeValidator
        regExp: /\d{4,}/
    }

    Component {
        id: dialogComponent

        Dialog {
            id: changeSecurityDialog
            objectName: "changeSecurityDialog"

            function displayMismatchWarning() {
                /* If the entry have the same length and different content,
                       display the non matching warning, if they do have the
                       same value then don't display it*/
                if (newInput.text.length === confirmInput.text.length)
                    if (newInput.text !== confirmInput.text)
                        notMatching.visible = true
                    else
                        notMatching.visible = false
            }

            // This is a bit hacky, but the contents of this dialog get so tall
            // that on a mako device, they don't fit with the OSK also visible.
            // So we scrunch up spacing.
            Binding {
                target: __foreground
                property: "itemSpacing"
                value: units.gu(1)
            }

            property int oldMethod
            property int newMethod

            signal accepted()

            title: {
                if (changeSecurityDialog.newMethod ==
                        changeSecurityDialog.oldMethod) { // Changing existing
                    switch (changeSecurityDialog.newMethod) {
                    case LomiriSecurityPrivacyPanel.Passcode:
                        return i18n.tr("Change passcode…")
                    case LomiriSecurityPrivacyPanel.Passphrase:
                        return i18n.tr("Change passphrase…")
                    default: // To stop the runtime complaining
                        return ""
                    }
                } else {
                    switch (changeSecurityDialog.newMethod) {
                    case LomiriSecurityPrivacyPanel.Swipe:
                        return i18n.tr("Switch to swipe")
                    case LomiriSecurityPrivacyPanel.Passcode:
                        return i18n.tr("Switch to passcode")
                    case LomiriSecurityPrivacyPanel.Passphrase:
                        return i18n.tr("Switch to passphrase")
                    }
                }
            }

            Label {
                text: {
                    switch (changeSecurityDialog.oldMethod) {
                    case LomiriSecurityPrivacyPanel.Passcode:
                        return i18n.tr("Existing passcode")
                    case LomiriSecurityPrivacyPanel.Passphrase:
                        return i18n.tr("Existing passphrase")
                        // Shouldn't be reached when visible but still evaluated
                    default:
                        return ""
                    }
                }

                visible: currentInput.visible
            }

            TextField {
                id: currentInput
                objectName: "currentInput"
                echoMode: TextInput.Password
                inputMethodHints: {
                    if (changeSecurityDialog.oldMethod ===
                            LomiriSecurityPrivacyPanel.Passphrase)
                        return Qt.ImhNoAutoUppercase | Qt.ImhSensitiveData
                    else if (changeSecurityDialog.oldMethod ===
                             LomiriSecurityPrivacyPanel.Passcode)
                        return Qt.ImhNoAutoUppercase |
                                Qt.ImhSensitiveData |
                                Qt.ImhDigitsOnly
                    else
                        return Qt.ImhNone
                }
                visible: changeSecurityDialog.oldMethod ===
                         LomiriSecurityPrivacyPanel.Passphrase ||
                         changeSecurityDialog.oldMethod ===
                         LomiriSecurityPrivacyPanel.Passcode
                onTextChanged: {
                    if (changeSecurityDialog.newMethod ===
                            LomiriSecurityPrivacyPanel.Swipe)
                        confirmButton.enabled = text.length > 0
                }
                Component.onCompleted: {
                    if (securityPrivacy.securityType !== LomiriSecurityPrivacyPanel.Swipe)
                        forceActiveFocus()
                }
            }

            /* Using bindings since it is, according to documentation,
            impossible to unset both validator and maximumLength properties */
            Binding {
                target: currentInput
                property: "validator"
                value:  passcodeValidator
                when: changeSecurityDialog.oldMethod ===
                      LomiriSecurityPrivacyPanel.Passcode
            }

            Binding {
                target: currentInput
                property: "maximumLength"
                value:  12
                when: changeSecurityDialog.oldMethod ===
                      LomiriSecurityPrivacyPanel.Passcode
            }

            Label {
                id: incorrect
                text: ""
                visible: text !== ""
                color: "darkred"
            }

            Label {
                text: {
                    switch (changeSecurityDialog.newMethod) {
                    case LomiriSecurityPrivacyPanel.Passcode:
                        return i18n.tr("Choose passcode")
                    case LomiriSecurityPrivacyPanel.Passphrase:
                        return i18n.tr("Choose passphrase")
                        // Shouldn't be reached when visible but still evaluated
                    default:
                        return ""
                    }
                }
                visible: newInput.visible
            }

            TextField {
                id: newInput
                objectName: "newInput"
                echoMode: TextInput.Password
                inputMethodHints: {
                    if (changeSecurityDialog.newMethod ===
                            LomiriSecurityPrivacyPanel.Passphrase)
                        return Qt.ImhNoAutoUppercase | Qt.ImhSensitiveData
                    else if (changeSecurityDialog.newMethod ===
                             LomiriSecurityPrivacyPanel.Passcode)
                        return Qt.ImhNoAutoUppercase |
                                Qt.ImhSensitiveData |
                                Qt.ImhDigitsOnly
                    else
                        return Qt.ImhNone
                }
                visible: changeSecurityDialog.newMethod ===
                         LomiriSecurityPrivacyPanel.Passcode ||
                         changeSecurityDialog.newMethod ===
                         LomiriSecurityPrivacyPanel.Passphrase
                onTextChanged: { displayMismatchWarning() }
                Component.onCompleted: {
                    if (securityPrivacy.securityType === LomiriSecurityPrivacyPanel.Swipe)
                        forceActiveFocus()
                }
            }

            /* Using bindings since it is, according to documentation,
            impossible to unset both validator and maximumLength properties */
            Binding {
                target: newInput
                property: "validator"
                value: passcodeValidator
                when: changeSecurityDialog.newMethod ===
                      LomiriSecurityPrivacyPanel.Passcode
            }

            Binding {
                target: newInput
                property: "maximumLength"
                value:  12
                when: changeSecurityDialog.newMethod ===
                      LomiriSecurityPrivacyPanel.Passcode
            }

            Label {
                text: {
                    switch (changeSecurityDialog.newMethod) {
                    case LomiriSecurityPrivacyPanel.Passcode:
                        return i18n.tr("Confirm passcode")
                    case LomiriSecurityPrivacyPanel.Passphrase:
                        return i18n.tr("Confirm passphrase")
                        // Shouldn't be reached when visible but still evaluated
                    default:
                        return ""
                    }
                }
                visible: confirmInput.visible
            }

            TextField {
                id: confirmInput
                echoMode: TextInput.Password
                inputMethodHints: {
                    if (changeSecurityDialog.newMethod ===
                            LomiriSecurityPrivacyPanel.Passphrase)
                        return Qt.ImhNoAutoUppercase | Qt.ImhSensitiveData
                    else if (changeSecurityDialog.newMethod ===
                             LomiriSecurityPrivacyPanel.Passcode)
                        return Qt.ImhNoAutoUppercase |
                                Qt.ImhSensitiveData |
                                Qt.ImhDigitsOnly
                    else
                        return Qt.ImhNone
                }
                visible: changeSecurityDialog.newMethod ===
                         LomiriSecurityPrivacyPanel.Passcode ||
                         changeSecurityDialog.newMethod ===
                         LomiriSecurityPrivacyPanel.Passphrase
                onTextChanged: { displayMismatchWarning() }
            }

            /* Using bindings since it is, according to documentation,
            impossible to unset both validator and maximumLength properties */
            Binding {
                target: confirmInput
                property: "validator"
                value:  passcodeValidator
                when: changeSecurityDialog.newMethod ===
                      LomiriSecurityPrivacyPanel.Passcode
            }

            Binding {
                target: confirmInput
                property: "maximumLength"
                value:  12
                when: changeSecurityDialog.newMethod ===
                      LomiriSecurityPrivacyPanel.Passcode
            }

            Label {
                id: notMatching
                wrapMode: Text.Wrap
                text: {
                    if (changeSecurityDialog.newMethod ===
                            LomiriSecurityPrivacyPanel.Passcode)
                        return i18n.tr("Those passcodes don't match. Try again.")
                    if (changeSecurityDialog.newMethod ===
                            LomiriSecurityPrivacyPanel.Passphrase)
                        return i18n.tr("Those passphrases don't match. Try again.")

                    //Fallback to prevent warnings. Not displayed.
                    return ""
                }
                visible: false
                color: "darkred"
            }

            RowLayout {
                spacing: units.gu(1)

                Button {
                    Layout.fillWidth: true
                    text: i18n.tr("Cancel")
                    onClicked: {
                        PopupUtils.close(changeSecurityDialog);

                        unlockMethod.setMethod(securityPrivacy.securityType)
                    }
                }

                Button {
                    id: confirmButton
                    Layout.fillWidth: true
                    color: theme.palette.normal.positive

                    text: {
                        if (changeSecurityDialog.newMethod ===
                                LomiriSecurityPrivacyPanel.Swipe)
                            return i18n.tr("Unset")
                        else if (changeSecurityDialog.oldMethod ===
                                 changeSecurityDialog.newMethod)
                            return i18n.tr("Change")
                        else
                            return i18n.tr("Set")
                    }
                    /* see https://wiki.ubuntu.com/SecurityAndPrivacySettings#Phone for details */
                    enabled: /* Validate the old method, it's either swipe or a secret which needs
                                to be valid, > 4 digits for the passcode or > 0 for a passphrase */
                             (changeSecurityDialog.oldMethod === LomiriSecurityPrivacyPanel.Swipe ||
                              ((changeSecurityDialog.oldMethod === LomiriSecurityPrivacyPanel.Passcode &&
                                currentInput.text.length >= 4) ||
                               (changeSecurityDialog.oldMethod === LomiriSecurityPrivacyPanel.Passphrase &&
                                currentInput.text.length > 0))) &&
                             /* Validate the new auth method, either it's a passcode and the code needs to be at least 4 digits */
                             ((changeSecurityDialog.newMethod === LomiriSecurityPrivacyPanel.Passcode &&
                               newInput.text.length >= 4 && confirmInput.text.length >= 4) ||
                              /* or a passphrase and then > 0 */
                              (changeSecurityDialog.newMethod === LomiriSecurityPrivacyPanel.Passphrase &&
                               newInput.text.length > 0 && confirmInput.text.length > 0) ||
                              /* or to be swipe */
                              changeSecurityDialog.newMethod === LomiriSecurityPrivacyPanel.Swipe)

                    onClicked: {
                        changeSecurityDialog.enabled = false
                        incorrect.text = ""

                        var match = (newInput.text == confirmInput.text)
                        notMatching.visible = !match
                        if (!match) {
                            changeSecurityDialog.enabled = true
                            newInput.forceActiveFocus()
                            newInput.selectAll()
                            return
                        }

                        var errorText = securityPrivacy.setSecurity(
                                    currentInput.visible ? currentInput.text : "",
                                    newInput.text,
                                    changeSecurityDialog.newMethod)

                        if (errorText !== "") {
                            incorrect.text = errorText
                            currentInput.forceActiveFocus()
                            currentInput.selectAll()
                            changeSecurityDialog.enabled = true

                            // We can always safely disable FP ident here, but
                            // in some cases it is required.
                            securityPrivacy.enableFingerprintIdentification = false;
                        } else {
                            changeSecurityDialog.accepted()
                        }
                    }
                }
            }
        }
    }

    Component {
        id: fingerprintPage
        Fingerprints {
        }
    }

    onActiveChanged: {
        if (active) {
            sizeObserver.start();
        } else {
            sizeObserver.stop();
        }
    }

    onEnrolledFingerprintsChanged: {
        // in case user reset its fingerprints and we still have fingerprint unlock set
        if (enrolledFingerprints === 0 && securityPrivacy.enableFingerprintIdentification) {
            securityPrivacy.enableFingerprintIdentification = false
        }
    }

    Flickable {
        id: scrollWidget
        anchors.fill: parent
        contentHeight: contentItem.childrenRect.height
        boundsBehavior: (contentHeight > page.height) ?
                            Flickable.DragAndOvershootBounds :
                            Flickable.StopAtBounds
        /* Set the direction to workaround
           https://bugreports.qt-project.org/browse/QTBUG-31905 otherwise the UI
           might end up in a situation where scrolling doesn't work */
        flickableDirection: Flickable.VerticalFlick

        Column {
            id: lockArea
            anchors.left: parent.left
            anchors.right: parent.right

            SettingsItemTitle {
                text: i18n.tr("Unlock the device using:")
            }

            ListItems.ItemSelector {
                id: unlockMethod
                expanded: true
                model: [
                    { text: i18n.tr("Swipe (no security)"), method: LomiriSecurityPrivacyPanel.Swipe},
                    { text: i18n.tr("Passcode..."), method: LomiriSecurityPrivacyPanel.Passcode},
                    { text: i18n.tr("Passphrase..."), method: LomiriSecurityPrivacyPanel.Passphrase}
                ]

                function setMethod(method) {
                    selectedIndex = model.findIndex(element => element.method === method)
                }

                delegate: OptionSelectorDelegate {
                    text: modelData.text
                }
                selectedIndex: model.findIndex(element => element.method === securityPrivacy.securityType)
                onDelegateClicked: {
                    const requestedMethod = model[index].method
                    if (securityPrivacy.securityType === LomiriSecurityPrivacyPanel.Passcode && securityPrivacy.securityType === requestedMethod) {
                        page.openPinPromptsPage()
                    } else{
                        openDialog(requestedMethod)
                    }
                }
            }

            SettingsListItems.SingleControl {

                id: changeControl
                visible: securityPrivacy.securityType !== LomiriSecurityPrivacyPanel.Swipe

                Button {
                    property string changePasscode: i18n.tr("Change passcode…")
                    property string changePassphrase: i18n.tr("Change passphrase…")

                    property bool passcode: securityPrivacy.securityType ===
                                            LomiriSecurityPrivacyPanel.Passcode

                    objectName: "changePass"
                    enabled: securityPrivacy.securityType !== LomiriSecurityPrivacyPanel.Swipe

                    text: passcode ? changePasscode : changePassphrase
                    width: parent.width - units.gu(4)

                    onClicked: passcode ?  openDialog(LomiriSecurityPrivacyPanel.Passcode) : openDialog(LomiriSecurityPrivacyPanel.Passphrase)
                }
                showDivider: true
            }

            Column {
                visible: Biometryd.available
                width: parent.width

                SettingsItemTitle {
                    text: i18n.tr("Biometrics:")
                }

                SettingsListItems.Standard {
                    id: fingerPrintToggle
                    text: i18n.tr("Fingerprint")
                    layout.subtitle.text: {
                        if (page.enrolledFingerprints === 0) {
                            return i18n.tr("No fingerprints set")
                        }
                        return ""
                    }

                    enabled: page.enrolledFingerprints > 0 && securityPrivacy.securityType !== LomiriSecurityPrivacyPanel.Swipe
                    showDivider: false

                    Switch {
                        checked: securityPrivacy.enableFingerprintIdentification
                        onClicked: securityPrivacy.enableFingerprintIdentification = !securityPrivacy.enableFingerprintIdentification
                    }
                }

                SettingsListItems.SingleValue {
                    id: fingerPrintHint
                    visible: securityPrivacy.securityType === LomiriSecurityPrivacyPanel.Swipe
                    layout.summary.text: i18n.tr("Fingerprint can only be used with passcode or password unlock method")
                }

                SettingsListItems.SingleValueProgression {
                    text: i18n.tr("Fingerprint ID")
                    visible: securityPrivacy.securityType !== LomiriSecurityPrivacyPanel.Swipe

                    onClicked: {
                        pageStack.addPageToNextColumn(page, fingerprintPage, {
                                                          passcodeSet: securityPrivacy.securityType !== LomiriSecurityPrivacyPanel.Swipe
                                                      })
                    }
                }
            }
        }
    }

    // This observer is used to decide whether or not to enabled Fingerprint ID
    // to be selected by the user as a security method.
    Observer {
        id: sizeObserver
        onFailed: {
            page.enrolledFingerprints = 0;
            _op = null;
        }
        onSucceeded: {
            page.enrolledFingerprints = result;
            _op = null;
        }
        onCanceled: {
            console.log('onCanceled')
            _op = null
        }

        function start () {
            if (Biometryd.available) {
                _op = Biometryd.defaultDevice.templateStore.size(user);
                _op.start(sizeObserver);
            }
        }

        function stop () {
            if (Biometryd.available && _op) {
                _op.cancel()
            }
        }

        property var _op: null

        Component.onDestruction: stop();
    }

    Connections {
        target: Biometryd
        onAvailableChanged: {
            if (available)
                sizeObserver.start();
        }
    }

    User {
        id: user
        uid: LomiriSettingsFingerprint.uid
    }
}
